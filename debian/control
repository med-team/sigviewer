Source: sigviewer
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Yaroslav Halchenko <debian@onerussian.com>,
           Michael Hanke <michael.hanke@gmail.com>,
           Alois Schlögl <alois.schloegl@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               qtbase5-dev,
               qttools5-dev,
               libfftw3-dev,
               libboost-dev,
               libbiosig-dev,
               libxdf-dev (>= 0.99.6+dfsg-3~),
               libsuitesparse-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/sigviewer
Vcs-Git: https://salsa.debian.org/med-team/sigviewer.git
Homepage: https://github.com/cbrnr/sigviewer
Rules-Requires-Root: no

Package: sigviewer
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: GUI viewer for biosignals such as EEG, EMG, and ECG
 SigViewer is a viewing and scoring software for biomedical signal
 data.  It relies on biosig4c++ library which supports a number of
 data formats (including EDF, BDF, GDF, BrainVision, BCI2000, CFWB,
 HL7aECG, SCP_ECG (EN1064), MFER, ACQ, CNT(Neuroscan), DEMG, EGI,
 EEG1100, FAMOS, SigmaPLpro, TMS32). The complete list of supported
 file formats is available at
 http://pub.ist.ac.at/~schloegl/biosig/TESTED .
 .
 Besides displaying biosignals, SigViewer supports creating
 annotations to select artifacts or specific events.
